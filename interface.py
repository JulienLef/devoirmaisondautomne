from tkinter import *
from generateurdenom import *

def click_valider():
    label_generateur.configure(text="Votre nom serais")
    label_nom.configure(text=str(nom.get()))
    label_prenom.configure(text=str(prenom.get()))
    
    label_nom.grid_forget()
    label_prenom.grid_forget()
    confirm_bouton.grid_forget()
    rb_Pirate.grid_forget()
    rb_OnePiece.grid_forget()
    rb_HarryPotter.grid_forget()

    return_bouton.grid(column=1,row=4)

    if(rb_valeur_coche.get()=='1'):
        nom.set(nomx1(nom.get()))
        prenom.set(prenomx1(prenom.get()))
    if(rb_valeur_coche.get()=='2'):
        nom.set(nomx2(nom.get()))
        prenom.set(prenomx2(prenom.get()))
    if(rb_valeur_coche.get()=='3'):
        nom.set(nomx3(nom.get()))
        prenom.set(prenomx3(prenom.get()))

def click_return():
    label_generateur.configure(text="Quel serais votre nom ?")
    label_nom.configure(text="Saisir votre nom")
    label_prenom.configure(text="Saisir votre prenom")
    nom.set("")
    prenom.set("")

    label_nom.grid(column=0,row=2)
    label_prenom.grid(column=2,row=2)
    confirm_bouton.grid(column=1, row=4)
    rb_Pirate.grid(column=0,row=1)
    rb_OnePiece.grid(column=1,row=1)
    rb_HarryPotter.grid(column=2,row=1)

    return_bouton.grid_forget()


frame = Tk()
frame.title("Générateur de Nom")

nom = StringVar() 
nom.set("")

prenom = StringVar() 
prenom.set("")

label_generateur=Label(frame,text="Quel serais votre nom ?")
label_generateur.grid(column=1,row=0)

label_nom=Label(frame,text="Saisir votre nom")
label_nom.grid(column=0,row=2)

label_prenom=Label(frame,text="Saisir votre prenom")
label_prenom.grid(column=2,row=2)

input_nom = Entry(frame, textvariable=nom)
input_nom.grid(column=0, row=3)

input_prenom = Entry(frame, textvariable=prenom)
input_prenom.grid(column=2, row=3)

confirm_bouton=Button(frame, text="Valider", command=click_valider)
confirm_bouton.grid(column=1,row=4)

return_bouton=Button(frame, text="Retour", command=click_return)
return_bouton.grid_forget()

rb_valeur_coche = StringVar() 
rb_Pirate = Radiobutton(frame, text="Pirate", variable=rb_valeur_coche, value=1)
rb_OnePiece = Radiobutton(frame, text="One Piece", variable=rb_valeur_coche, value=2)
rb_HarryPotter = Radiobutton(frame, text="Harry Potter", variable=rb_valeur_coche, value=3)
rb_Pirate.grid(column=0,row=1)
rb_OnePiece.grid(column=1,row=1)
rb_HarryPotter.grid(column=2,row=1)
rb_Pirate.select()

frame.mainloop()
