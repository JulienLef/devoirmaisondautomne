import unittest
from generateurdenom import nomx1
from generateurdenom import prenomx1

class TestGeneratuerdenomFunctions(unittest.TestCase):
    def test_nomx1(self) :
        self.assertEqual(nomx1("Delahaye"), "Jack")
        self.assertEqual(nomx1("mASson"), "Flibustier")

    def test_prenomx1(self) :
        self.assertEqual(prenomx1("Xavier"), "Morgan")
        self.assertEqual(prenomx1("jEaN-François"), "Le Dur")
