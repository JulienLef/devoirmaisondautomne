import string

def nomx1(texte) :
    data = {'dict' : {'A' : 'Capitaine' , 'B' : 'Tom' , 'C' : 'Second' , 'D' : 'Jack' , 'E' : 'Barbe' , 'F' : 'Will' , 'G' : 'Moussaillon' , 'H' : 'Canonnier' , 'I' : 'Matelot' , 'J' : 'John' , 'K' : 'Commandant' , 'L' : 'Lieutenant' , 'M' : 'Flibustier' , 'N' : 'Bill' , 'O' : 'Anne' , 'P' : 'Woodes' , 'Q' : 'Charles' , 'R' : 'Benjamin' , 'S' : 'Mary' , 'T' : 'Edward' , 'U' : 'Olivier' , 'V' : 'Bartholomew' , 'W' : 'Samuel' , 'X' : 'Robert' , 'Y' : 'Charles' , 'Z' : 'Francis' }}

    name = texte[0]
    name = name.upper()
    chaine = data['dict'][name]

    return chaine

def nomx2(texte) :
    data = {'dict' : {'A' : 'Monkey D' , 'B' : 'Roronoa' , 'C' : 'Vinsmoke' , 'D' : 'Tony-Tony' , 'E' : 'Nico' , 'F' : 'Gol D' , 'G' : 'Silvers' , 'H' : 'Trafalgar D Water' , 'I' : 'Eustass' , 'J' : 'Scratchmen' , 'K' : 'Basil' , 'L' : 'Charlotte' , 'M' : 'Capone' , 'N' : 'Jewelry' , 'O' : 'Newgate' , 'P' : 'Portgas D' , 'Q' : 'Beckman' , 'R' : 'Roo' , 'S' : 'Marshall D' , 'T' : 'Jesus' , 'U' : 'Haguar D' , 'V' : 'Doctor' , 'W' : 'Lucci' , 'X' : 'Emporio' , 'Y' : 'Dracule' , 'Z' : 'Boa' }}

    name = texte[0]
    name = name.upper()    
    chaine = data['dict'][name]

    return chaine

def nomx3(texte) :
    data = {'dict' : {'A' : 'Abbot' , 'B' : 'Black' , 'C' : 'Crabbe' , 'D' : 'Dumbledore' , 'E' : 'Eskivdur' , 'F' : 'Flamel' , 'G' : 'Granger' , 'H' : 'Hagrid' , 'I' : 'Jedusor' , 'J' : 'Krum' , 'K' : 'Londubat' , 'L' : 'MacGonagall' , 'M' : 'Malefoy' , 'N' : 'Sir' , 'O' : 'Ombrage' , 'P' : 'Potter' , 'Q' : 'Quirrell' , 'R' : 'Rogue' , 'S' : 'Skeeter' , 'T' : 'Thicknesse' , 'U' : 'Vance' , 'V' : 'Veridian' , 'W' : 'Weasley' , 'X' : 'Miss' , 'Y' : 'Yaxley' , 'Z' : 'Zabini' }}

    name = texte[0]
    name = name.upper()
    chaine = data['dict'][name]

    return chaine

def prenomx1(texte) :
    data = {'dict' : {'A' : 'Hadock' , 'B' : 'Noir' , 'C' : 'Barberousse' , 'D' : 'Le Timide' , 'E' : 'Sparow' , 'F' : 'Fracasse' , 'G' : 'Caverne' , 'H' : 'Rouge' , 'I' : 'Le Cruel' , 'J' : 'Le Dur' , 'K' : 'Le Borgne' , 'L' : 'Bleu' , 'M' : 'Turner' , 'N' : 'Gold' , 'O' : 'Bonny' , 'P' : 'Rodgers' , 'Q' : 'Vane' , 'R' : 'Hornigold' , 'S' : 'Read' , 'T' : 'Teach' , 'U' : 'Levasseur' , 'V' : 'Roberts' , 'W' : 'Bellamy' , 'X' : 'Morgan' , 'Y' : 'Surcouf' , 'Z' : 'Drake' }}

    name = texte[0]
    name = name.upper()
    chaine = data['dict'][name]

    return chaine

def prenomx2(texte) :
    data = {'dict' : {'A' : 'Luffy' , 'B' : 'Zoro' , 'C' : 'Sanji' , 'D' : 'Chopper' , 'E' : 'Robin' , 'F' : 'Roger' , 'G' : 'Rayleigh' , 'H' : 'Law' , 'I' : 'Kidd' , 'J' : 'Apoo' , 'K' : 'Hawkins' , 'L' : 'Chiffon' , 'M' : 'Bege' , 'N' : 'Bonney' , 'O' : 'Edward' , 'P' : 'Ace' , 'Q' : 'Ben' , 'R' : 'Lucky' , 'S' : 'Teach' , 'T' : 'Burgess' , 'U' : 'Sauro' , 'V' : 'Vegapunk' , 'W' : 'Rob' , 'X' : 'Ivankov' , 'Y' : 'Mihawk' , 'Z' : 'Hancock' }}

    name = texte[0]
    name = name.upper()
    chaine = data['dict'][name]

    return chaine

def prenomx3(texte) :
    data = {'dict' : {'A' : 'Hannah' , 'B' : 'Sirius' , 'C' : 'Vincent' , 'D' : 'Albus' , 'E' : 'Wilbert' , 'F' : 'Nicolas' , 'G' : 'Hermione' , 'H' : 'Rubeus' , 'I' : 'Tom' , 'J' : 'Viktor' , 'K' : 'Neuville' , 'L' : 'Minerva' , 'M' : 'Drago' , 'N' : 'Nicholas' , 'O' : 'Dolores' , 'P' : 'Harry' , 'Q' : 'Quirinus' , 'R' : 'Severus' , 'S' : 'Rita' , 'T' : 'Pius' , 'U' : 'Emmeline' , 'V' : 'Vindictus' , 'W' : 'Ron' , 'X' : 'Teigne' , 'Y' : 'Corban' , 'Z' : 'Blaise' }}

    name = texte[0]
    name = name.upper()
    chaine = data['dict'][name]

    return chaine
